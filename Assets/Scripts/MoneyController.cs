﻿using UnityEngine;
using System.Collections;

public class MoneyController : MonoBehaviour {

	public float gunsPerSec;
	public int gunsPerClick;
	public float gunValue;
	public float gunCount;
	public int crateSize;
	public float crateWorth;
	public int workerCount;
	public int gold;
	public float money;
	public int upgradeCount;
	public int workerOutput;
	public float oneSecond = 1.0f;
	public int[] gunTypes;

	void Start () {
		getSaveData ();
		repeat ();
	}

	public void gunClicked(){
		gunCount = gunCount + gunsPerClick;
		PlayerPrefs.SetFloat ("gunCount", gunCount);
	}

	void Update () {
	if (Input.GetKeyDown ("m")) {
			PlayerPrefs.SetFloat ("money", 1000000);
		}
		getSaveData ();
		gunsPerSec = workerOutput * workerCount;
		PlayerPrefs.SetFloat ("gunsPerSec", gunsPerSec);
		if (gunCount >= crateSize){
			gunCount = gunCount - crateSize;
			money = money + crateWorth;
			PlayerPrefs.SetFloat ("money", money);
			PlayerPrefs.SetFloat ("gunCount", gunCount);
		}
}

	void repeat(){
		InvokeRepeating ("gunsAddPerSec", 1, 0.1f);
	}

	public void gunsAddPerSec(){
		gunCount = gunCount + gunsPerSec / 10;
		PlayerPrefs.SetFloat ("gunCount", gunCount);
	}

	public void getSaveData(){
		gunsPerSec = PlayerPrefs.GetInt ("gunsPerSec");
		gunsPerClick = PlayerPrefs.GetInt ("gunsPerClick");
		gunValue = PlayerPrefs.GetFloat ("gunValue");
		gunCount = PlayerPrefs.GetFloat ("gunCount");
		crateSize = PlayerPrefs.GetInt ("crateSize");
		crateWorth = PlayerPrefs.GetFloat ("crateWorth");
		workerCount = PlayerPrefs.GetInt ("workerCount");
		workerOutput = PlayerPrefs.GetInt ("workerOutput");
		gold = PlayerPrefs.GetInt("goldCount");
		money = PlayerPrefs.GetFloat ("money");
		upgradeCount = PlayerPrefs.GetInt ("upgradeCount");
	}
}
