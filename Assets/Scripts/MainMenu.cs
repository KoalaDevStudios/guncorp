﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public string[] upgradeTags;
	public GameObject resetScreen;
	public GameObject pauseMenu;

	public void Reset(){
		Debug.Log ("Reset request");
		resetScreen.SetActive (true);
	}
		
	public void valueReset(){
		for (int i = 0; i <= upgradeTags.Length; i++) {
			PlayerPrefs.SetInt("" + i + "currentAmount", 0);
			PlayerPrefs.SetFloat ("" + i + "currentCost", 0);
		}
		PlayerPrefs.SetInt ("totalUpgrades", 0);
		PlayerPrefs.SetFloat ("gunsPerSec", 0);
		PlayerPrefs.SetInt ("gunsPerClick", 1);
		PlayerPrefs.SetFloat ("gunValue", 100);
		PlayerPrefs.SetInt ("gunCount", 0);
		PlayerPrefs.SetInt ("crateSize", 15);
		PlayerPrefs.SetFloat ("crateWorth", 1500);
		PlayerPrefs.SetInt ("workerCount", 0);
		PlayerPrefs.SetFloat ("money", 0);
		PlayerPrefs.SetInt ("fillingCrate", 0);
		PlayerPrefs.SetInt ("workerOutput", 1);
		}

	public void testingPurposes(){
		valueReset();
		PlayerPrefs.SetInt("goldCount", 0);
	}

	public void resetCheckYes(){
		Debug.Log ("Confirmed reset");
		valueReset ();
		resetScreen.SetActive (false);
		pauseMenu.SetActive (false);
	}

	public void resetCheckNo(){
		Debug.Log ("Not resetting");
		resetScreen.SetActive (false);
	}


}