﻿using UnityEngine;
using System.Collections;

public class MainUI : MonoBehaviour {

	public GameObject pauseCanvas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PauseMenu(){
		pauseCanvas.SetActive (true);
		Time.timeScale = 0;
	}


}
