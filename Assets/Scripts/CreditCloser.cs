﻿using UnityEngine;
using System.Collections;

public class CreditCloser : MonoBehaviour {

	public GameObject creditsScreen;

	public void closeCredits(){
		creditsScreen.SetActive (false);
	}

	public void openCredits(){
		creditsScreen.SetActive (true);
	}
}
