﻿using UnityEngine;
using System.Collections;

public class ShopManger : MonoBehaviour {

	public GameObject shopFrame;
	public void closeShop(){
		shopFrame.SetActive (false);
	}

	public void openShop(){
		shopFrame.SetActive (true);
	}
}
