﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gunsPSCounter : MonoBehaviour {
	private Text text;
	public float gunsPS;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		gunsPS = PlayerPrefs.GetFloat ("gunsPerSec");
	}
	
	// Update is called once per frame
	void Update () {
		gunsPS = PlayerPrefs.GetFloat ("gunsPerSec");
		text.text = "" + gunsPS;
	}
}
