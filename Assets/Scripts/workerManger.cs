﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class workerManger : MonoBehaviour {
	public int currentAmount;
	public int maxAmount;
	public float defaultCost;
	public float cost;
	public float costMultiplier;
	public Text costText;
	public float money;
	public int workersToAdd;
	public Slider capCheck;
	private int totalUpgrades;
	public Button button;
	public int workerCount;
	public int workerOutput;
	public string upgradeTag;
	public float oldCost;
	public string newCostString;
	// Use this for initialization
	void Start () {
		cost = PlayerPrefs.GetFloat ("" + upgradeTag + "currentCost");
		currentAmount = PlayerPrefs.GetInt ("" + upgradeTag + "currentAmount");
		oldCost = cost;
		currencyConvert ();
		if (cost <= 0) {
			cost = defaultCost;
			costText.text = "" + cost;
		}
	}
	
	// Update is called once per frame
		void Update () {
			money = PlayerPrefs.GetFloat ("money");
			cost = PlayerPrefs.GetFloat ("" + upgradeTag + "currentCost");
			currentAmount = PlayerPrefs.GetInt (""+upgradeTag+"currentAmount");
			totalUpgrades = PlayerPrefs.GetInt ("totalUpgrades");
			PlayerPrefs.SetFloat ("" + upgradeTag + "currentCost", cost);
			PlayerPrefs.SetInt ("" + upgradeTag + "currentAmount", currentAmount);
			if (currentAmount == maxAmount) {
				button.interactable = false;
				capCheck.value = currentAmount;
			} else if (currentAmount < maxAmount){
				capCheck.value = currentAmount;
				button.interactable = true;
			}
			if (cost == 0) {
				cost = defaultCost;
			}
			if (currentAmount == 0) {
				costText.text = "" + defaultCost;

			}
		}

	public void workerAdd(){
		if (money >= cost & currentAmount < maxAmount) {
			money = money - cost;
			PlayerPrefs.SetFloat ("money", money);
			workerCount = workerCount + workersToAdd;
			PlayerPrefs.SetInt ("workerCount", workerCount);
			totalUpgrades = totalUpgrades + 1;
			PlayerPrefs.SetInt ("totalUpgrades", totalUpgrades);
			currentAmount++;
			PlayerPrefs.SetInt ("" + upgradeTag + "currentAmount", currentAmount);
			capCheck.value = currentAmount;
			if (currentAmount == maxAmount) {
				button.interactable = false;
			} else if (currentAmount < maxAmount) {
				cost = cost * costMultiplier;
				cost = Mathf.RoundToInt (cost);
				currencyConvert ();
				PlayerPrefs.SetFloat ("" + upgradeTag + "currentCost", cost);
			}
		} else {
			Debug.Log ("Either not affordable or error not accounted for");
		}
		}

	public void doubleOutput(){
		if (money >= cost & currentAmount < maxAmount) {
			currentAmount++;
			PlayerPrefs.SetInt ("" + upgradeTag + "currentAmount", currentAmount);
			capCheck.value = currentAmount;
			money = money - cost;
			PlayerPrefs.SetFloat ("money", money);
			workerOutput = PlayerPrefs.GetInt ("workerOutput");
			workerOutput = workerOutput * 2;
			PlayerPrefs.SetInt ("workerOutput", workerOutput);
			capCheck.value = currentAmount;
			totalUpgrades = totalUpgrades + 1;
			PlayerPrefs.SetInt ("totalUpgrades", totalUpgrades);
			if (currentAmount > maxAmount) {
				cost = cost * costMultiplier;
				cost = Mathf.RoundToInt (cost);
				currencyConvert ();
				PlayerPrefs.SetFloat ("" + upgradeTag + "currentCost", cost);
			}
		}
	}


	public void currencyConvert(){
		if (cost >= Mathf.Pow(10, 33f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 33f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " D";
		} else if (cost >= Mathf.Pow(10, 30f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 30f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " N";
		} else if (cost >= Mathf.Pow(10, 27f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 27);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " O";
		} else if (cost >= Mathf.Pow(10, 24f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 24);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Sep";
		} else if (cost >= Mathf.Pow(10, 21f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 21f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Sex"; 
			cost = oldCost;
		} else if (cost >= Mathf.Pow(10, 18f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 18f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Qui";
		} else if (cost >= Mathf.Pow(10, 15f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 15f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Qua";
		} else if (cost >= Mathf.Pow(cost, 12f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 12f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " T";
		} else if (cost >= Mathf.Pow(10, 9f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 9f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " B";
		} else if (cost >= Mathf.Pow(10, 6f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 6f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " M";
		} else if (cost >= 100000) {
			oldCost = cost;
			oldCost = oldCost / 1000;
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " K";
		} else {
			Debug.Log ("No need to convert");
			costText.text = "" + cost;  
		}

	}
}

			
