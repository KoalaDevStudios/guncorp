﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyCounter : MonoBehaviour {

	private Text text;
	public float money = 0f;
	public string newMoneyString;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		money = PlayerPrefs.GetFloat ("money");
	}
	void Update () {
		money = PlayerPrefs.GetFloat ("money");
		if (money >= 1000000000) {
			money = money / 1000000000;
			newMoneyString = money.ToString ("F2");
			text.text = "" + newMoneyString + " B";
		} else if (money >= 1000000) {
			money = money / 1000000;
			newMoneyString = money.ToString ("F2");
			text.text = "" + newMoneyString + " M";
		} else if (money >= 100000){
			money = money / 1000;
			newMoneyString = money.ToString("F2");
			text.text = "" + newMoneyString + " K";
		} else {
			text.text = "" + money;
		}
	}
}
