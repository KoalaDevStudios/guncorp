﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {


	public string menu;
	public GameObject pauseCanvas;
	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ResumeGame(){
		Time.timeScale = 1;
		pauseCanvas.SetActive (false);
	}

	public void MainMenu(){
		SceneManager.LoadScene (menu);
	}


}
