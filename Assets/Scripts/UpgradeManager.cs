﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour {

	public int currentAmount;
	public int maxAmount;
	public float cost;
	public float costMultiplier;
	public Text costText;
	public float money;
	public Slider capCheck;
	public float gunValue;
	public float improvementAmount;
	private int totalUpgrades;
	public Button button;
	public float crateWorth;
	public int crateSize;
	public string upgradeTag;
	public int defaultCost;
	public string newCostString;
	public float oldCost;

	// Use this for initialization
	void Start () {
		cost = PlayerPrefs.GetFloat ("" + upgradeTag + "currentCost");
		oldCost = cost;
		currentAmount = PlayerPrefs.GetInt (""+upgradeTag+"currentAmount");
		currencyConvert ();

	}
	
	// Update is called once per frame
	void Update () {
		cost = PlayerPrefs.GetFloat ("" + upgradeTag + "currentCost");
		currentAmount = PlayerPrefs.GetInt (""+upgradeTag+"currentAmount");
		gunValue = PlayerPrefs.GetFloat ("gunValue");
		totalUpgrades = PlayerPrefs.GetInt ("totalUpgrades");
		crateSize = PlayerPrefs.GetInt ("crateSize");
		crateWorth = gunValue * crateSize;
		PlayerPrefs.SetFloat ("crateWorth", crateWorth);
		PlayerPrefs.SetFloat ("" + upgradeTag + "currentCost", cost);
		PlayerPrefs.SetInt ("" + upgradeTag + "currentAmount", currentAmount);
		if (currentAmount == maxAmount) {
			button.interactable = false;
			capCheck.value = currentAmount;
		} else if (currentAmount < maxAmount){
			capCheck.value = currentAmount;
			button.interactable = true;
		}
		if (cost == 0) {
			cost = defaultCost;
		}
		if (currentAmount == 0) {
			costText.text = "" + defaultCost;

		}
	}

	public void purchased(){
		money = PlayerPrefs.GetFloat ("money");
		if (money >= cost & currentAmount < maxAmount) {
			money = money - cost;
			PlayerPrefs.SetFloat ("money", money);
			currentAmount++;
			PlayerPrefs.SetInt ("" + upgradeTag + "currentAmount", currentAmount);
			capCheck.value = currentAmount;
			totalUpgrades = totalUpgrades + 1;
			PlayerPrefs.SetInt ("totalUpgrades", totalUpgrades);
			capCheck.value = currentAmount;
			gunValue = gunValue * improvementAmount;
			gunValue = Mathf.RoundToInt (gunValue);
			PlayerPrefs.SetFloat ("gunValue", gunValue);
			cost = cost * costMultiplier;
			cost = Mathf.Round (cost);
			currencyConvert ();
			PlayerPrefs.SetFloat ("" + upgradeTag + "currentCost", cost);
			if (currentAmount == maxAmount) {
				button.interactable = false;
				capCheck.value = maxAmount;
			}
		} else if (money < cost) {
			Debug.Log ("CannotAffordUpgrade");
		} else {
			Debug.Log ("Unable to purchase for reason not catered for");
		}
	}


	void currencyConvert(){
		if (cost >= Mathf.Pow(10, 33f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 33f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " D";
		} else if (cost >= Mathf.Pow(10, 30f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 30f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " N";
		} else if (cost >= Mathf.Pow(10, 27f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 27);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " O";
		} else if (cost >= Mathf.Pow(10, 24f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 24);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Sep";
		} else if (cost >= Mathf.Pow(10, 21f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 21f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Sex"; 
			cost = oldCost;
		} else if (cost >= Mathf.Pow(10, 18f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 18f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Qui";
		} else if (cost >= Mathf.Pow(10, 15f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 15f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " Qua";
		} else if (cost >= Mathf.Pow(cost, 12f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 12f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " T";
		} else if (cost >= Mathf.Pow(10, 9f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 9f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " B";
		} else if (cost >= Mathf.Pow(10, 6f)) {
			oldCost = cost;
			oldCost = oldCost / Mathf.Pow(10, 6f);
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " M";
		} else if (cost >= 100000) {
			oldCost = cost;
			oldCost = oldCost / 1000;
			newCostString = oldCost.ToString ("F2");
			costText.text = "" + newCostString + " K";
		} else {
			Debug.Log ("No need to convert");
			costText.text = "" + cost;  
		}

	}

}
